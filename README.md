# TP javascript

premier TP en javascript

> - Auteur : Kevin COLMANT
> - Date : 06/04/2021

## Sommaire

- Introduction
  - [HelloWorld](Introduction/Helloworld_js/index.html)
- Variables et opérateurs
  - [Surface](variable_et_operateur/Surface/index.html)
  - [IMC](variable_et_operateur/IMCvariable/index.html)
  - [Convertisseur](variable_et_operateur/ConvertirDegFar/index.html)
- Les structures de contrôle
  - [Syracuse](StructureControle/Syracuse/index.html)
  - [IMC](StructureControle/IMC2/index.html)
  - [CalculFactorielle](StructureControle/CalculFactorielle/index.html)
  - [Conversion Euros/Dollars](StructureControle/ConvEuroDollars/index.html)
  - [Nombres Triples](StructureControle/NombresTriples/index.html)
  - [Suite de Fibonacci](StructureControle/SuiteFibonacci/index.html)
  - [Table de multiplication](StructureControle/TableMultiplication/index.html)
- Les fonctions en Javascript
  - [IMC](Fonctions/IMC3/index.html)
  - [Temps de Trajet](Fonctions/tempsTrajet/index.html)
  - [Recherche du nombre de multiples de 3](Fonctions/Multiple3/index.html)
- Les objets en JavaScript
  - [IMC](Objets/IMCobj/index.html)
  - [IMC Objet Version 2](Objets/IMCobj2/index.html)
  - [IMC Objet Constructeur](Objets/IMCobj2V2/index.html)
  - [Héritage Prototype](Objets/Heritageproto/index.html)
  - [Personnage De Jeu](Objets/Persojeux/index.html)