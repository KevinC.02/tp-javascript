//Colmant Kevin 
//11/05/2021
//Exercice sur l'heritage des prototypes avec des professeurs et des élèves

function Personne(prmNom, prmPrenom, prmAge, prmSexe) { //Fonction pour la personne
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}

Personne.prototype.decrire = function() {
    let informations; //variable contenant les infos de la personne
    informations = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return informations;
}

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) { //Fonction pour les professeurs
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

Professeur.prototype = Object.create(Personne.prototype); //Objet Personne initialisé dans le prototype

function eleve(prmNom, prmPrenom, prmAge, prmSexe,prmclasse) { //Fonction pour les élèves
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmclasse; 
    
}

eleve.prototype = Object.create(Personne.prototype); //Objet Personne initialisé dans le prototype

Professeur.prototype.decrire_plus = function() { //Fonction pour le professeur
    let informations;
    let prefixe;    //Variable contenant des mots celon les sexes
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'Mr'; //Masculin
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'Mme'; //Féminin 
    }
    informations = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;//Phrase du professeur a affiché
    return informations;
}

eleve.prototype.decrire_plus = function() {  //Fonction pour l'élève 
    let informations;
    let prefixe;
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'un élève'; //Masculin
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'une élève'; //Féminin
    }
    informations =this.prenom + " " + this.nom + " est "+prefixe +" de "+ this.classe; //Phrase de l'élève a affiché
    return informations;
}
//Ajout d'un nouveau professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'masculin', 'Mathématiques'); 
//Résultat affiché dans la concole de commande
console.log(objProfesseur1.decrire()); //affichage des informations du professeur
console.log(objProfesseur1.decrire_plus()); //informations complémentaire du professeur
//Ajout d'un nouvel élève
let objeleve1 = new eleve('Dutillieul','Dorian',18,'Masculin','SNIR1');  
//Résultat affiché dans la concole de commande
console.log(objeleve1.decrire());//affichage des informations de l'élève
console.log(objeleve1.decrire_plus());//informations complémentaire de l'élève
