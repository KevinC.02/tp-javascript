//Colmant Kevin 
//27/04/2021
//Création d'un constructeur d'objet utilisé pour l'IMC 

function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
//Initialisations des informations des patients
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille;
    this.poids = prmPoids;

    this.decrire = function () {
        let description = "";
        let tailleMetre = 0;
        let sexe = this.sexe;
        tailleMetre = this.taille / 100
        if (sexe == 'masculin') {   //Masculin
            description = "le patient " + this['prenom'] + " " + this['nom'] + " est agé de " + this['age'] + "ans. Il mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        } else {    //Féminin
            description = "la patiente " + this['prenom'] + " " + this['nom'] + " est agé de " + this['age'] + "ans. elle mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        }
        return description;
    }

    this.definir_corpulence = function () {
        let poids = 0;
        let taille = 0;
        let imc = 0;
        let corpulence = "";    //Initialisation de la phrase de fin 
        let sexe = this.sexe;  //Sexe de la personne
        poids = this.poids;     // Stockage du poids
        taille = this.taille;   // Stockage de la taille 

        function calculer_IMC() {
            let valIMC;
            valIMC = poids / ((taille / 100) * (taille / 100)); //Calcul de l'IMC pour les patients
            return valIMC;
        };

        function interpreter_IMC() {
            let IMCsexe;
            let interpretation = "";

            if (sexe == 'masculin') {
                IMCsexe = imc - 2;      //Masculin
            } else {
                IMCsexe = imc;      //Féminin
            }

            if (IMCsexe < 16.5) { // Insuffisance pondérale
                interpretation = "Insuffisance pondérale";
            }
            if ((IMCsexe >= 16.5) && (IMCsexe < 18.5)) { //Maigreur
                interpretation = "Maigreur";
            }
            if ((IMCsexe >= 18.5) && (IMCsexe < 25)) { //Poids normal
                interpretation = "Poids normal";
            }
            if ((IMCsexe >= 25) && (IMCsexe < 30)) { //Surpoids
                interpretation = "Surpoids";
            }
            if ((IMCsexe >= 30) && (IMCsexe < 35)) { //Obésité
                interpretation = "Obésité ";
            }
            if ((IMCsexe >= 35) && (IMCsexe <= 40)) { //Obésité sévère
                interpretation = "Obésité sévère";
            }
            if (IMCsexe > 40) { //Obésité morbide
                interpretation = "Obésité morbide";
            }
            return interpretation;
        };
        imc = calculer_IMC();
        //Pour l'interprétation de l'IMC celon le sexe
        if (sexe == 'masculin') {
            corpulence = "Son IMC est de : " + imc.toFixed(2) + "\n" + "il est en " + interpreter_IMC(imc)//Masculin
        } else {
            corpulence = "Son IMC est de : " + imc.toFixed(2) + "\n" + "elle est en " + interpreter_IMC(imc)//Féminin
        }
        return corpulence;
    }


};

//Objets contenant les différents patients 
let objPatient1 = new patient('Dupont', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new patient('Moulin', 'Isabelle', 46, 'feminin', 158, 74);
let objPatient3 = new patient('Martin', 'Eric', 42, 'masculin', 165, 90);

//Résultat des objets affichés dans la console de commande  
console.log(objPatient1.decrire());              //informations du patient 1
console.log(objPatient1.definir_corpulence());   //Interprétation de l'IMC du patient 1 avec sa catégorie 
console.log(objPatient2.decrire());              //informations du patient 2
console.log(objPatient2.definir_corpulence());   //Interprétation de l'IMC du patient 2 avec sa catégorie 
console.log(objPatient3.decrire());              //informations du patient 3
console.log(objPatient3.definir_corpulence());   //Interprétation de l'IMC du patient 3 avec sa catégorie 
