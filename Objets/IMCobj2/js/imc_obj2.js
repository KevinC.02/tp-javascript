//Colmant Kevin
//27/04/2021
//Création d'un objet littéral pour le calcul de l'IMC version 2

//Initialisation de l'objet Patient
let objPatient = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    //Déclaration des fonctions 
    decrire: function () {
        let description;    //Description de la personne 
        let convMetres = 0;
        convMetres = this.taille / 100; // Convertisseur de la taille en mètre
        description = "le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] + " ans. Il mesure " + convMetres + " mètre et pèse " + this['poids'] + " kg";
        return description;
    },

    definir_corpulence: function () {
        //Déclaration des variables
        let poids = 0;  
        let taille = 0; 
        let imc = 0;    
        let corpulence = "";    //Initialisation de la phrase de fin  
        poids= this.poids ;     // Stockage du poids
        taille = this.taille ;  // Stockage de la taille

        function calculer_IMC() {
            let valIMC;
            valIMC = poids / ((taille / 100) * (taille / 100)); // Calcul de l'IMC de la personne
            return valIMC;
        };

        function interpreter_IMC(prmIMC) {
            let interpretation = "";

            if (prmIMC < 16.5) { //Insuffisance ponderale
                interpretation = "Insuffisance ponderale";
            }
            if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //Maigreur
                interpretation = "Maigreur";
            }
            if ((prmIMC >= 18.5) && (prmIMC < 25)) { //Poids normal
                interpretation = "Poids normal";
            }
            if ((prmIMC >= 25) && (prmIMC < 30)) { //Surpoids
                interpretation = "Surpoids";
            }
            if ((prmIMC >= 30) && (prmIMC < 35)) { //Obésité
                interpretation = "Obésité";
            }
            if ((prmIMC >= 35) && (prmIMC <= 40)) { //Obésité sévère
                interpretation = "Obésité sévère";
            }
            if (prmIMC > 40) { //Obésité morbide
                interpretation = "Obésité morbide";
            }
            return interpretation;
        };
        imc = calculer_IMC(); //Initialisation de la fonction dans la variable imc
        corpulence = "Son IMC est de : " + imc.toFixed(2) +"\n"+"il est en situation de "+ interpreter_IMC(imc) //Phrase de fin a affiché
        return corpulence;
    }
};

//Résultat affiché dans la console de commande 
console.log(objPatient.decrire())
console.log(objPatient.definir_corpulence())


