## readme pour les objets sur les personnages de jeux

![Images_Test_Objet_Personnages_Jeux](images/perso.png)

# Autre personnage hors du sujet 

![Images_Test_Objet_Personnages_Jeux_2](images/perso2.png)

# Photo du guerrier

![Images_Test_Objet_Personnages_Jeux_3](images/perso3.png)

# Photo magicien

![Images_Test_Objet_Personnages_Jeux_4](images/perso4.png)