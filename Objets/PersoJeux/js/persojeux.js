//Colmant Kevin
//11/05/2021
//Création de personnages avec des objets
function personnage(prmNom, PrmNiveau) {
    //Informations principales du personnage 
    this.Nom = prmNom;
    this.Niveau = PrmNiveau;
}

personnage.prototype.saluer = function () { //Fonction por montrer que le personnage vous salue
    let saluer;
    saluer = this.Nom + " vous salue!!"  //Phrase pour saluer
    return saluer;
}

function guerrier(prmNom, prmNiveau, prmArmes) { //Fonction contenant les guerriers 
    personnage.call(this, prmNom, prmNiveau)
    this.Armes = prmArmes;
}

guerrier.prototype = Object.create(personnage.prototype); //Initialisation de l'objet dans le prototype du guerrier

function Magicien(prmNom, prmNiveau, prmSorts) { //Fonction contenant les magiciens 
    personnage.call(this, prmNom, prmNiveau)
    this.Sorts = prmSorts;
}

Magicien.prototype = Object.create(personnage.prototype); //Initialisation de l'objet dans le prototype du magicien

guerrier.prototype.combattre = function () {
    let réplique = "";
    réplique = this.Nom + ' est un guerrier qui se bat avec ' + this.Armes; //Phrase affiché pour le guerrier
    return réplique;
}

Magicien.prototype.posseder = function () {
    let réplique = "";
    réplique = this.Nom + ' est un magicien qui posséde le pouvoir de ' + this.Sorts;//Phrase affiché pour le magicien
    return réplique;
}
//Création des personnages 
let objperso1 = new guerrier('Arthur', 3, 'une épée');
let objperso2 = new Magicien('Merlin', 2, 'prédire les batailles');
//Affichage dans la console de commannde
console.log(objperso1.saluer());
console.log(objperso1.combattre());
console.log(objperso2.saluer());
console.log(objperso2.posseder());
