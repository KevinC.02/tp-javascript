//Colmant Kevin
//27/04/2021
//Création d'un objet littéral pour le calcul de l'IMC 

//Initialisation de l'objet Patient
let objPatient = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    //Déclaration des fonctions 
    decrire: function () {
        let description;    //Description de la personne 
        let convMetres = 0;
        convMetres = this.taille / 100; // Convertisseur de la taille en mètre
        description = "le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] + " ans. Il mesure " + convMetres + " mètre et pèse " + this['poids'] + " kg";
        return description;
    },

    calculer_IMC: function () {
        let valIMC;
        valIMC = this.poids / ((this.taille / 100) * (this.taille / 100)); // Calcul de l'IMC de la personne
        return valIMC;
    },

    interpreter_IMC: function (prmIMC) {
        let interpretation = "";

        if (prmIMC < 16.5) { //Insuffisance ponderale
            interpretation = "Insuffisance ponderale";
        }
        if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //Maigreur
            interpretation = "Maigreur";
        }
        if ((prmIMC >= 18.5) && (prmIMC < 25)) { //Poids normal
            interpretation = "Poids normal";
        }
        if ((prmIMC >= 25) && (prmIMC < 30)) { //Surpoids
            interpretation = "Surpoids";
        }
        if ((prmIMC >= 30) && (prmIMC < 35)) { //Obésité
            interpretation = "Obésité";
        }
        if ((prmIMC >= 35) && (prmIMC <= 40)) { //Obésité sévère
            interpretation = "Obésité sévère";
        }
        if (prmIMC > 40) { //Obésité morbide
            interpretation = "Obésité morbide";
        }
        return interpretation;
    }

};


let resIMC = 0; // Valeur de l'IMC
resIMC = objPatient.calculer_IMC();// Fonction initialisé dans l'objet pour utiliser la valeur de l'IMC 

//Résultat affiché dans la console
console.log(objPatient.decrire());
console.log("Son IMC est de : " + objPatient.calculer_IMC().toFixed(2));
console.log("il est en situation de " + objPatient.interpreter_IMC(resIMC));
