//Colmant Kevin 
//22/04/2021
//Calcul de la surface 

//Declaration des variables
let longueur = 5; // Longueur 
let largeur = 6; //  Largeur 
let surface = 0; //  Surface

surface = longueur * largeur; // Calcul de la surface

//Resultat affiché sur la console 
console.log("longueur de la piece : " + longueur + "m")
console.log("largeur de la piece : " + largeur + "m")
console.log("surface de la piece : " + surface + "m²")
