//Colmant Kevin
//07/04/2021
//Calcul de l'IMC d'une personne en fonction de sa taille et de son poids 
console.log("Calcul de l'IMC d'une personne");
let taille = 178;
let poids = 58;
let imc = poids / ((taille / 100) * (taille / 100));    //Calcul de l'IMC

console.log("taille :" + taille + "cm") //affiche la taille dans la console 
console.log("poids :" + poids + "kg")   //affiche le poids dans la console 
console.log("IMC :" + imc.toFixed(1) + "kg/m²")    //affiche l'IMC dans la console 

