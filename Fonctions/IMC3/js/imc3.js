//Colmant Kevin
//27/04/2021
//Calculer l'IMC donné avec une fonction imbriquée

//Déclaration de la fonction CalculerIMC
function calculerIMC(prmTaille, prmPoids) {
    let valeurIMC;
    valeurIMC = prmPoids / ((prmTaille / 100) * (prmTaille / 100)); //Calcul d'un IMC
    return valeurIMC;
}
//Déclaration de la fonction interpreterIMC
function interpreterIMC(prmIMC) {
    let interpretation = "";
    if (prmIMC < 16.5) { //Insuffisance ponderale ou dénutrition
        interpretation = " Insuffisance ponderale";
    }
    if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //Maigreur
        interpretation = " Maigreur";
    }
    if ((prmIMC >= 18.5) && (prmIMC < 25)) { //Poids normal ou corpulence normale
        interpretation = " Poids normal ";
    }
    if ((prmIMC >= 25) && (prmIMC < 30)) { //Surpoids
        interpretation = " Surpoids";
    }
    if ((prmIMC >= 30) && (prmIMC < 35)) { //Obésité 
        interpretation = " Obesite";
    }
    if ((prmIMC >= 35) && (prmIMC <= 40)) { //Obésité sévère
        interpretation = " Obesite severe";
    }
    if (prmIMC > 40) { //Obésité morbide
        interpretation = " Obesite morbide";
    }
    return interpretation;
}
//Déclaration des variables
let taille = 178;
let poids = 58;
let IMC = 0;
let interpretation = "";

IMC = calculerIMC(taille, poids);
interpretation = interpreterIMC(IMC);
//Résultat affiché dans la console
console.log("Calcul de l'IMC :")
console.log("Votre taille choisi : " + taille + "cm");
console.log("Votre poids choisi : " + poids + "kg");
console.log("Votre IMC est égal à : " + IMC.toFixed(1) + "      Vous êtes en état de" + interpretation);
