//Colmant Kevin
//27/04/2021
//Calcul du temps de parcours d'un trajet défini en heures, minutes et secondes

//Fonction pour calculer le temps du parcours 
function calculerTempsParcours(prmVitesse, prmDistance) {
    let tpsSec = 0;
    tpsSec = (prmDistance / prmVitesse) * 3600; //Calcul du temps en seconde
    return tpsSec;
}
//Fonction pour convertir le temps 
function convertir_h_min_sec(prmTps) {
    let heure = 0;
    let min = 0;
    let sec = 0;
    let affichage = "";

//Calcul de conversion 
    heure = prmTps / 3600;
    min = (prmTps % 3600) / 60;
    sec = (prmTps % 3600) % 60;

    affichage = Math.floor(heure) + " h " + Math.floor(min) + " min " + sec + " s";
    return affichage;
}
//Déclaration des variables
let vitesse = 90;
let distance = 500;
let tpsSec = 0;
let tpsHeureMinSec = 0; //Heure totale avec heures,minutes et secondes

tpsSec = calculerTempsParcours(vitesse, distance)
tpsHeureMinSec = convertir_h_min_sec(tpsSec);

//Affichage dans la console de commande 
console.log("Calcul du temps de parcours d'un trajet : ");
console.log("Vitesse moyenne (km/h) : " + vitesse);
console.log("Distance à parcourir (km) : " + distance);
console.log("Pour une vitesse de " + vitesse + "km/h, une distance de " + distance + " km est parcourue en " + tpsHeureMinSec);

