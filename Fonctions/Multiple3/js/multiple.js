//Colmant Kevin 
//27/04/2021
//Affichage des valeurs multiple de 3 entre 0 et un nombre au choix 

//Déclaration des variables 
let value = 20;            //Valeur du nombre choisi (fin d'intervalle)
let multiple = 0;
let affichage = "";

//Fonction pour rechercher les multiples de 3 entre cette intervalle
function rechercher_Mult3(prmValue) {

    for (let chiffre = 0; chiffre < prmValue; chiffre++) {
        if (chiffre % 3 == 0) { //Si le chiffre est divisible par 3
            affichage = affichage + chiffre + "-";  //Affichage des multiples
        }
    }
    return affichage;
}

affichage = rechercher_Mult3(value);

//Affichage dans la console de commande
console.log("recherche des multiple de 3 : ")
console.log("valeur limite de la recherche : 20 ")
console.log(affichage);
