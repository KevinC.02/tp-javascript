//Colmant Kevin
//22/04/2021
//Exercice utilisant le calcul factorielle

//Declaration des variables
let N = 3; //Nombre utilisant la factorielle
let resultat = 1;
let varAffichage = " " + resultat; 

//Boucle 
for (let i = 1; i <= N; i++) {
    resultat = i * resultat; // On multiplie le résulat par i
    varAffichage = varAffichage + " X " + i;
}
// Resultat affiché sur la console
console.log(N + "!=" + varAffichage + " = " + resultat)
