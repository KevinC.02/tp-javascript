//Colmant Kevin
//22/04/2021
//Calcul de l'IMC d'une personne en fonction de sa taille et de son poids 
console.log("Calcul de l'IMC d'une personne");
let taille = 178;
let poids = 58;
let imc = poids / ((taille / 100) * (taille / 100));    //Calcul de l'IMC

console.log("taille :" + taille + "cm") //affiche la taille dans la console 
console.log("poids :" + poids + "kg")   //affiche le poids dans la console 
console.log("IMC :" + imc.toFixed(1) + "kg/m²")    //affiche l'IMC dans la console 

if (imc < 16.5) {
    console.log("Catégorie de votre IMC: insuffisance ponderale");      //Affiche la catégorie dans laquelle vous êtes
}

if (imc < 18.5) {
    console.log("Catégorie de votre IMC: maigreur");    //Affiche la catégorie dans laquelle vous ête
}
else if (imc < 25) {
    console.log("Catégorie de votre IMC:  poids normal ");  //Affiche la catégorie dans laquelle vous ête
}
else if (imc < 30) {
    console.log("Catégorie de votre IMC: Surpoids");    //Affiche la catégorie dans laquelle vous ête
}
else if (imc < 35) {
    console.log("Catégorie de votre IMC: Obésité"); //Affiche la catégorie dans laquelle vous ête
}
else if (imc < 40) {
    console.log("Catégorie de votre IMC: Obésité sévère");  //Affiche la catégorie dans laquelle vous ête
}
else {
    console.log("Catégorie de votre IMC: Obésité morbide"); //Affiche la catégorie dans laquelle vous ête
}