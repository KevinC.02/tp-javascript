//Colmant Kevin
//25/04/2021
//Exercice de la suite de Syracuse 

let N = 5;         //N pour valeur de départ
let valeur = " " + N;
let nbTour = 0;     //Compteur du nombre de tours
let valeurMax = N;  // Valeur maximum de la chaine 

console.log("suite de syracuse pour " + N + ": ");

//Si N est inférieur à 1 alors on affichera un message d'erreur
if (N < 1) {
    console.log("erreur valeur de N inferieur a 1 ");
}

else {
    while (N > 1) {
        if (N > valeurMax) {
            valeurMax = N;      // La valeur max deviendra donc N 
        }
        if (N % 2 == 0) {
            N = N / 2;          //On le divise par 2 si le nombre de départ est pair
        }
        else {
            N = (N * 3) + 1;    //On le multplie par 3 
        }
        valeur = valeur + "-" + N; // Ajouter a la suite de la chaine de caractere la valeur N
        nbTour++; 
    }
}
//Resultat affiché sur la console 
console.log(valeur);
console.log("temps de vol = " + nbTour);
console.log("Altitude maximale = " + valeurMax);
