//Colmant Kevin
//22/04/2021
//Conversion de l'Euro vers le Dollars

//Declaration des variables
let values = 1; //Euro
let resultat = 1.65; //Dollars

//Tant que la valeur est inférieur ou égale à 16384
while (values <= 16384) {
    resultat = values * 1.65; // Calcul de la conversion de l'euro en dollars
    //Resultat affiché sur la console 
    console.log(values + " euro(s) = " + resultat.toFixed(2) + " dollar(s)");
    values = values * 2; //On multiplie par 2 pour aller vers la valeur suivante
}
