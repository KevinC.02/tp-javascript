//Kevin Colmant
//25/04/2021
//Exercice dont le but est d'additionner 2 termes qui précède un nombre

let nb1 = 0; //Première valeur
let nb2 = 1; //Deuxième valeur
let resultat = 0; //Resultat de l'addition des 2 nombres précédents
let affichage = "" + nb1 + " " + nb2;

//Boucle pour les 17 premiers chiffres 
for (let i = 0; i < 17; i++) {
    resultat = nb1 + nb2;
    nb1 = nb2; //remplacement de la première valeur par la valeur 2
    nb2 = resultat; //remplacement de la deuxième valeur par le resultat
    affichage = affichage + " " + resultat;
}
//Resultat indiqué Dans la console
console.log("voici la suite de fibonacci : " + affichage);
