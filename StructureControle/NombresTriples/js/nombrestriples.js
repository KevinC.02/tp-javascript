//Colmant Kevin 
//22/04/2021
//Exercice consistant à multiplier le nombre précédent par 3

//Declaration des variables
let nbDepart = 3 ; //Valeur choisie
let resultat = 0 ; // Resultat du calcul triple 
let affichage = " " + nbDepart ;
resultat = nbDepart ; 

//Boucle 
for(let i = 0; i < 12 ; i++){
    resultat = resultat * 3 ;
    affichage = affichage + " " + resultat;
}
// Resultat affiché sur la console de commande 
console.log("valeur de depart :" + nbDepart) ;
console.log(affichage) ;
