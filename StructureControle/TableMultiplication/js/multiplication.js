//Kevin Colmant
//25/04/2021
//Exercice pour obtenir les 20 premiers termes de la table de multiplication par 7 

let valBase = 7 ; //Valeur de base 
let resultat = 0 ;
let affichage = "" ;

//Boucle de la table de multiplication
for (let i = 1 ; i <= 20 ; i ++) {
    resultat = valBase * i ;

    if (resultat % 3 == 0 ){
        affichage = affichage  + resultat + " * " ;
    }else{
        affichage = affichage  + resultat + " " ;
    }
}
console.log(affichage) ;
